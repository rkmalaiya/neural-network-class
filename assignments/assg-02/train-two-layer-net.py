#!/usr/bin/env python
import numpy as np
import argparse
from nnassglib.classifiers.neural_net import TwoLayerNet
from nnassglib.data_utils import load_CIFAR10


def get_CIFAR10_data(num_training=49000, num_validation=1000, num_test=1000):
    """
    Load the CIFAR-10 dataset from disk and perform preprocessing to prepare
    it for the two-layer neural net classifier. These are the same steps as
    we used for the SVM, but condensed to a single function.  
    """
    # Load the raw CIFAR-10 data
    cifar10_dir = 'data/cifar-10-batches-py'
    X_train, y_train, X_test, y_test = load_CIFAR10(cifar10_dir)

    # Subsample the data
    mask = range(num_training, num_training + num_validation)
    X_val = X_train[mask]
    y_val = y_train[mask]
    mask = range(num_training)
    X_train = X_train[mask]
    y_train = y_train[mask]
    mask = range(num_test)
    X_test = X_test[mask]
    y_test = y_test[mask]

    # Normalize the data: subtract the mean image
    mean_image = np.mean(X_train, axis=0)
    X_train -= mean_image
    X_val -= mean_image
    X_test -= mean_image

    # Reshape data to rows
    X_train = X_train.reshape(num_training, -1)
    X_val = X_val.reshape(num_validation, -1)
    X_test = X_test.reshape(num_test, -1)

    return X_train, y_train, X_val, y_val, X_test, y_test


def train_network(learning_rate, num_hidden, regularization, num_iterations):
  """Train a two layer network (ReLU hidden layer followed by a softmax
  output layer) on the CIFAR-10 data.  We will train the network using
  the provided metaparameters, and display results from training to
  standard output.

  Parameters
  ---------
  learning_rate - The learning rate to use in training network.
  num_hidden - The number of hidden units to create in the network in the
     hidden layer.
  regularization - The value of regularization strength to use in training.
  """
  # load the data we will train and validate network with
  X_train, y_train, X_val, y_val, X_test, y_test = get_CIFAR10_data()
  print 'Train data shape: ', X_train.shape
  print 'Train labels shape: ', y_train.shape
  print 'Validation data shape: ', X_val.shape
  print 'Validation labels shape: ', y_val.shape
  print 'Test data shape: ', X_test.shape
  print 'Test labels shape: ', y_test.shape

  # create network with given parameters and train it
  input_size = 32 * 32 * 3
  num_classes = 10
  net = TwoLayerNet(input_size, num_hidden, num_classes)
  stats = net.train(X_train, y_train, X_val, y_val,
                    num_iters=num_iterations, batch_size=200,
                    learning_rate=learning_rate, learning_rate_decay=0.95,
                    reg=regularization, verbose=True)

  # evaluate the current classifier on train and validation set
  y_train_pred = net.predict(X_train)
  training_accuracy = np.mean(y_train == y_train_pred)
  print 'final training accuracy: %f' % (training_accuracy)
  y_val_pred = net.predict(X_val)
  validation_accuracy = np.mean(y_val == y_val_pred)
  print 'final validation accuracy: %f' % (validation_accuracy)

  # print out results summary, for ease of parsing/visualizing results from
  # many training sessions
  train_loss = stats['loss_history'][-1] # pull out the final loss from the history
  print "Results  num_iterations: %d num_hidden: %d learning_rate: %g regularization: %g train_loss: %g train_accuracy: %g validation_accuracy: %g" % (num_iterations, num_hidden, learning_rate, regularization, train_loss, training_accuracy, validation_accuracy)


def main():
  description = """Train two layer network (ReLU hidden layer with softmax output layer)
  on the CIFAR-10 data set using the given metaparameters
  """
  parser = argparse.ArgumentParser(description=description)
  parser.add_argument("learning_rate", type=float, help="learning rate to train with")
  parser.add_argument("num_hidden", type=int, help="number of units in hidden layer to train network with")
  parser.add_argument("regularization", type=float, help="regularization strength to train network with")
  parser.add_argument("num_iterations", type=int, help="number of training iterations to perform")
  args = parser.parse_args()
  train_network(args.learning_rate, args.num_hidden, args.regularization, args.num_iterations)


if __name__ == "__main__":
  main()
