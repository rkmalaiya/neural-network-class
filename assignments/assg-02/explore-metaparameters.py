#!/usr/bin/env python
import os
import numpy as np


def select_parameters_randomly():
    num_training_sessions = 100

    for session in range(num_training_sessions):
        learning_rate = 10 ** np.random.uniform(-4, 0)
        num_hidden = np.random.randint(10, 41) * 50
        regularization = 10 ** np.random.uniform(-3, 0)
        num_iterations = 2500
        cmd = "qsub train-two-layer-net-submit.qsub %e %d %e %d" % (learning_rate, num_hidden, regularization, num_iterations)
        print cmd
        os.system(cmd)


def main():
    select_parameters_randomly()


if __name__ == "__main__":
    main()
