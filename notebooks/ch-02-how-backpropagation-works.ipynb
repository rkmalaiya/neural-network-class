{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Chapter 2: How the Backpropagation Algorithm Works\n",
    "--------------------------------------------------\n",
    "\n",
    "Notebook to accompany lecture on Chapter 2 of Michael Nielsen's online Open Source book \n",
    "[\"Neural Networks and Deep Learning\"](http://neuralnetworksanddeeplearning.com/chap2.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from numpy import *\n",
    "from matplotlib import *\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Feedforward Activation: Matrix-Based Approach\n",
    "----------------------------------------------    \n",
    "\n",
    "The chapter begins by more formally introducing notation that can be used to specify both the feedforward activation function\n",
    "of a neural network composed of a number of layers of units (with weights and biases associated with each layer's units).  This\n",
    "notation is also used later when developing and discussing the fast backpropagation implementation, so we will look at it.  But\n",
    "basically, if you understood the use of lists to hold the weight matrix and bias vectors for each layer of a feedforward\n",
    "network in our previous notebook, then what is done here basically mirrors that concrete implementation.\n",
    "\n",
    "To refer to weights unambiguously in a multi-layer network, we will use the notation $w_{jk}^l$.  This denotes the\n",
    "weight for the connection from the $k^{th}$ neuron in the $(l -1)^{th}$ layer to the $j^{th}$ neuron in the\n",
    "$l^{th}$ layer.  So for example, the diagram below shows the weight on a connection from the fourth neuron\n",
    "in the second layer to the second neuron in the third layer of a network:\n",
    "\n",
    "<img src=\"files/figures/network-layer-notation.png\">\n",
    "\n",
    "As mentioned, one quirk of the notation is that you might feel that intuitively the ordering of the $j$ and $k$ indices\n",
    "should be reversed.  The reason for this is that if we specify the indices in this order, it makes the necessary\n",
    "matrix-vector multiplication for the weighted sums computation work in the normal way that we define matrix-vector\n",
    "multiplication.\n",
    "\n",
    "The notation for the biases vectors is similar.  We use $b_j^l$ to refer to the bias of the $j^{th}$ neuron in the\n",
    "$l^{th}$ layer.  And we will use $a_j^l$ for the activation of the $j^{th}$ neuron in the $l^{th}$ layer.\n",
    "\n",
    "With this notation, the activation $a_j^l$ of the $j^{th}$ neuron in the $l^{th}$ layer is related to the activations in the\n",
    "$(l - 1)^{th}$ layer by the equation (the same weighted sum plus bias feed into an activation function we have been\n",
    "doing previously):\n",
    "\n",
    "$$\n",
    "a_j^l = \\sigma \\Big( \\sum_k w_{jk}^l a_k^{l-1} + b_j^l \\Big)\n",
    "$$\n",
    "\n",
    "where the sum is over all neurons $k$ in the $(l - 1)^{th}$ layer.  We can think of the activation function $\\sigma$ as\n",
    "being vectorized, just as we have been using it in our previous notebook.  A vectorized funciton, when given a vector or\n",
    "a matrix, performs the function on each element in the vector, and returns the new corresponding transformed vector\n",
    "of values).  So if the activation function $\\sigma$ works on a vector of inputs, we can rewrite  the equation\n",
    "computing the activation for a single unit in a layer, to compute the activations for all units in a layer \n",
    "simultaneously as:\n",
    "\n",
    "**EQ 25:**\n",
    "$$\n",
    "a^l = \\sigma \\Big( w^l a^{l-1} + b^l  \\Big)\n",
    "$$\n",
    "\n",
    "This expression gives a much more global and compact way of thinking about how the activations spread in a network.\n",
    "We just apply the weight matrix to the activations, then add the bias vector, and finally apply the vectorized\n",
    "activation function $\\sigma$.  This global view, of activation as a layer by layer matrix and vector expression, is\n",
    "often easier and more succinct, then thinking in terms of neuron-by-neuron iterated operations.  By the way, as previously\n",
    "mentioned, its because we want to do the feed forward activation as shown here that we need to express the\n",
    "weights indices as $w_{jk}^l$, with $j$ and $k$ maybe reversed from how we would normally think of them.  With the\n",
    "indices expressed in this order, we can do the matrix-vector multiplication in the normal manner.\n",
    "\n",
    "When computing Equation 25, we compute the intermediate quantity $z^l \\equiv w^l a^{l-1} + b^l$ along the way\n",
    "(the weighted sum before we transform using the activation function).  This quantity is also useful in discussions\n",
    "of backpropagation and thus we call $z^l$ the *weighted input* to the neurons in layer $l$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assumptions Needed about the Cost Function\n",
    "------------------------------------------\n",
    "\n",
    "The goal of backpropagation is to compute the partial derivatives $\\partial C / \\partial w$ and $\\partial C / \\partial b$\n",
    "of the cost function $C$ with respect to any weight $w$ or bias $b$.  For backpropagation to work we need to make two\n",
    "main assumptions.  Recall the quadratic cost function we used previously (the sum of the squared errors):\n",
    "\n",
    "$$\n",
    "C = \\frac{1}{2n} \\sum_x || y(x) - a^L(x)||^2\n",
    "$$\n",
    "\n",
    "where $n$ is the total number of training examples, the sum is over individual training examples $x$, $y(x)$ is the\n",
    "corresponding desired (correct) output for pattern $x$, $L$ denotes the number of layers in the network and\n",
    "$a^L(x)$ is the vector of activations output from the network for input training pattern $x$.\n",
    "\n",
    "**Assumption 1**: The cost function needs to be able to be written as an average $C = \\frac{1}{n} \\sum_x C_x$\n",
    "over cost function $C_x$ for individual training examples $x$.  We need this assumption because what we actually\n",
    "compute are the partial derivatives for a single training example $x$  $\\partial C_x / \\partial w$ and\n",
    "$\\partial C_x / \\partial b$  We then determine the gradient with respect to all of the input patterns by \n",
    "averaging over training examples.\n",
    "\n",
    "**Assumption 2**: The cost needs to be able to be written as a function of the outputs from the neural network.\n",
    "This is certainly true of our example cost function, as we directly use the output activation vector $a_j^L$\n",
    "in computing the cost.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Hadamard Product $s \\odot t$\n",
    "--------------------------------\n",
    "\n",
    "The feedforward activation and the calculation of the gradients for the backpropagation algorithms are based on\n",
    "common linear algebraic operations - things like vector addition, multiplying a vector by a matrix, and so on.  But we make\n",
    "use of a less common operation in the discussion of the backpropagation algorithm.  The Hadamard product is simply the\n",
    "elementwise product of two vectors.  The components of two vectors of equal length $j$ using this elementwise\n",
    "product $s \\odot t$ are simply $s_j t_j$.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1]\n",
      " [2]] (2, 1)\n",
      "[[3]\n",
      " [4]] (2, 1)\n",
      "[[3]\n",
      " [8]] (2, 1)\n"
     ]
    }
   ],
   "source": [
    "s = np.array([[1], \n",
    "              [2]])\n",
    "t = np.array([[3], \n",
    "              [4]])\n",
    "print s, s.shape\n",
    "print t, t.shape\n",
    "print s * t, (s * t).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So notice that the default in the NumPy library is to do this elementwise multiplication when using the ~*~ operator.  In fact, if\n",
    "we want true matrix-vector or matrix-matrix multiplication (as defined in linear algebra), we need to use the ~np.dot()~ dot\n",
    "product function (as we have done in previous notebook)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Four Fundamental Equations Behind Backpropagation\n",
    "-----------------------------------------------------\n",
    "\n",
    "**Summary: The equations of backpropagation**\n",
    "\n",
    "**BP1**\n",
    "$$\n",
    "\\delta^L = \\triangledown_a C \\odot \\sigma'(z^L)\n",
    "$$\n",
    "\n",
    "Calculate an error term on the output (final) layer $L$.\n",
    "\n",
    "**BP2**\n",
    "$$\n",
    "\\delta^l = ((w^{l+1})^T \\delta^{l+1}) \\odot \\sigma' (z^l)\n",
    "$$\n",
    "\n",
    "Backpropagate to calculate errors on previous layers.\n",
    "\n",
    "**BP3**\n",
    "$$\n",
    "\\frac{\\partial C}{\\partial b_j^l} = \\delta_j^l\n",
    "$$\n",
    "\n",
    "Use the errors to calculate gradient with respect to the bias terms on the nodes.\n",
    "\n",
    "**BP4**\n",
    "$$\n",
    "\\frac{\\partial C}{\\partial w_{jk}^l} = a_k^{l-1} \\delta_j^l\n",
    "$$\n",
    "\n",
    "Use the errors to calculate the gradient with respect to the weights between the nodes.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Backpropagation is about understanding how changing the weights and biases in a network changes the cost function.  It is an\n",
    "iterative (hill-climbing) approach that attempts to find a minimum of the cost function by exploring the cost function space.\n",
    "Ultimately, this means computing the gradients as represented by the partial derivatives $\\partial C / \\partial w_{jk}^l$\n",
    "and $\\partial C / \\partial b_j^l$  To compute those we first introduce an intermediate quantity $\\delta_j^l$, which we call\n",
    "the *error* in the $j^{th}$ neuron in the $l^{th}$ layer.  Backpropagation will give us a procedure to compute the\n",
    "error $\\delta_j^l$, and then will relate $\\delta_j^l$ to $\\partial C / \\partial w_{jk}^l$ and $\\partial C / \\partial b_j^l$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Backpropagation Algorithm\n",
    "-----------------------------\n",
    "\n",
    "1. **Input** $x$: Set the corresponding activation $a$ for the input layer.\n",
    "1. **Feedforward**: For each $l = 2, 3, ..., L$ compute $z^l = w^l a^{l-1} + b^l$ and $a^l = \\sigma(z^l)$\n",
    "1. **Output error** $\\delta^L$: Compute the vector $\\delta^L = \\triangledown_a C \\odot \\sigma'(z^L)$\n",
    "1. **Backpropagate the error**: For each $l = L-1, L-2, ..., 2$ compute $\\delta^l = ((w^{l+1})^T \\delta^{l+1}) \\odot \\sigma'(z^l)$\n",
    "1. **Output**: The gradient of the cost function is given by $\\frac{\\partial C}{\\partial w_{jk}^l} = a_k^{l-1} \\delta_j^l$ and $\\frac{\\partial C}{\\partial b_j^l} = \\delta_j^l$\n",
    "\n",
    "The algorithm starts by calculating the activations for an input through the normal feedforward activation mechanism of the\n",
    "network.  We then start by computing the error vectors on the final layer $\\delta^L$, and we backpropagate to compute the\n",
    "errors on the preceeding layers.  From the errors, we can calculate the gradient with respect to the cost function for any\n",
    "weigh or bias term in the network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using Batch Training\n",
    "--------------------\n",
    "\n",
    "The preceeding basic algorithm is using online training, e.g. we compute the gradient of the cost function for each single\n",
    "individual training example, $C = C_x$.  In practice it is often faster to combine backpropagation with a learning algorithm\n",
    "such as stochastic gradient descent, in which we compute the gradient for many training examples.  In particular, given a mini-batch\n",
    "of $m$ training examples, the following algorithm applies a gradient descent learning step based on that mini-batch:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
