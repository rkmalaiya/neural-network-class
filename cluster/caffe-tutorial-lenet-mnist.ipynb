{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training LeNet on MNIST with Caffee\n",
    "\n",
    "A tutorial from the official Caffe library site:\n",
    "\n",
    "http://caffe.berkeleyvision.org/gathered/examples/mnist.html\n",
    "\n",
    "Shows how to set up and train the LeNet CNN architecture:\n",
    "\n",
    "http://yann.lecun.com/exdb/mnist/\n",
    "\n",
    "http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Preparation\n",
    "\n",
    "This network uses the MNIST training images. The official tutorial used some prebuilt binaries in order to download\n",
    "and preprocess the data files into lmdb databases.  The exact commands used for data preparation preprocessing \n",
    "in the tutorial were:\n",
    "\n",
    "```\n",
    "cd $CAFFE_ROOT\n",
    "./data/mnist/get_mnist.sh\n",
    "./examples/mnist/create_mnist.sh\n",
    "```\n",
    "\n",
    "These require you have the full Caffe environment compiled and installed, and the shell scripts only run on\n",
    "a unix/linux system.  If you look at these shell scripts, the get_mnist script simply downloads \n",
    "the gziped data from a remote location, and the create_mnist script is using some caffe binaries \n",
    "to create the lmdb database of the images.\n",
    "\n",
    "Here we present an equivalent python script/code to convert the 10k test images and 50k training images in the\n",
    "standard MNIST data set into an lmdb database suitable for training by Caffe.\n",
    "\n",
    "We first load the same raw mnist data we were using for our deep learning textbook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# first load the training images and correct outputs into numpy arrays for processing\n",
    "import cPickle, gzip\n",
    "\n",
    "f = gzip.open('../notebooks/data/mnist.pkl.gz', 'rb')\n",
    "train_set, valid_set, test_set = cPickle.load(f)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a reminder, the resulting data is simply a bunch of numpy arrays.  Each of the 3 variables \n",
    "`train_set`, `valid_set` and `test_set` are simply a tuple of two numpy arrays, the training imags\n",
    "and the training labels for each one.  The training set contains 50,000 training images with labels,\n",
    "the validation set contains another 10,000 images, and the test set also contains 10,000 images.  We\n",
    "will only use the training set for training, and the test set to test our training performance in\n",
    "this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# just a reminder, each of the above is a tuple of numpy.ndarrays.  For example, the train_set\n",
    "# is a tuple of inputs x and outputs y.\n",
    "train_images, train_labels = train_set\n",
    "test_images, test_labels = test_set\n",
    "\n",
    "print type(train_images), len(train_images)\n",
    "print type(train_labels), len(train_labels)\n",
    "\n",
    "# the inputs are 784 length greyscale values of the original 28x28 input images\n",
    "print train_images.shape\n",
    "\n",
    "# but the outputs are encoded as a single decimal number 0-9, which we will have to convert into a one-hot vector\n",
    "print train_labels.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One slight adjustment we need for our data.  The LeNet network definition we are using from the Caffe tutorial\n",
    "basically assumes that the images are shaped as 28x28 pixel images (easy enough to fix), but also assumes that\n",
    "each pixel is represented as an unsigned integer values (e.g. a value with 8 bits, ranging from 0 to 255).  So\n",
    "we first reshape and recast the train_images and test_images to the required shape and type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "# the pixel values are in range 0.0 - 1.0, recast them into range 0.0 - 255.0\n",
    "train_images *= 255.0\n",
    "test_images *= 255.0\n",
    "\n",
    "# now we reshape and recast into an unsigned int data type, for storage in the lmdb database\n",
    "IMAGE_WIDTH = 28\n",
    "IMAGE_HEIGHT = 28\n",
    "train_images = train_images.reshape(len(train_labels), IMAGE_WIDTH, IMAGE_HEIGHT).astype(np.uint8)\n",
    "test_images = test_images.reshape(len(test_labels), IMAGE_WIDTH, IMAGE_HEIGHT).astype(np.uint8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# lets just show the first training image after converting to uint8\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.imshow(train_images[0,:,:], cmap=plt.cm.gray_r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is apparently common practice for Caffe work to place all of the images in either a lmdb or leveldb\n",
    "database.  These are high performance databases, optimized to reduce disk I/O so can train with large data\n",
    "sets and not have to store complete set of images in the core memory.\n",
    "[Here](http://deepdish.io/2015/04/28/creating-lmdb-in-python/) is a quick description of\n",
    "the lmdb database usage.  \n",
    "\n",
    "Here we write some quick and dirty code to create  lmdb databases holding the training and testing data\n",
    "respecrively, and write each of our images with its corresponding label into the database.  Apparently \n",
    "Caffe stores the label as a simple integer in the database, and then internally it takes care of converting\n",
    "the final layer of 10 outputs as a one-hot vector representation to compare with the integer label we store\n",
    "here in the lmdb database.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import caffe\n",
    "from caffe.proto import caffe_pb2\n",
    "import lmdb\n",
    "\n",
    "# put all train images/labels in train_lmdb database\n",
    "train_lmdb = 'data/mnist/train_lmdb'\n",
    "in_db = lmdb.open(train_lmdb, map_size=int(1e12))\n",
    "with in_db.begin(write=True) as in_txn:\n",
    "    for idx, img in enumerate(train_images):\n",
    "        datum = caffe_pb2.Datum()\n",
    "        datum.channels = 1\n",
    "        datum.height = 28\n",
    "        datum.width = 28\n",
    "        datum.data = img.tobytes()\n",
    "        datum.label = train_labels[idx]        \n",
    "        str_id = '{:08}'.format(idx)\n",
    "        in_txn.put(str_id, datum.SerializeToString())\n",
    "in_db.close()\n",
    "\n",
    "# now put all test images/labels in test_lmdb database\n",
    "test_lmdb = 'data/mnist/test_lmdb'\n",
    "in_db = lmdb.open(test_lmdb, map_size=int(1e12))\n",
    "with in_db.begin(write=True) as in_txn:\n",
    "    for idx, img in enumerate(test_images):\n",
    "        datum = caffe_pb2.Datum()\n",
    "        datum.channels = 1\n",
    "        datum.height = 28\n",
    "        datum.width = 28\n",
    "        datum.data = img.tobytes()\n",
    "        datum.label = test_labels[idx]        \n",
    "        str_id = '{:08}'.format(idx)\n",
    "        in_txn.put(str_id, datum.SerializeToString())\n",
    "in_db.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model Definition\n",
    "\n",
    "Caffe models are defined in a prototxt format file.  The prototxt is the \n",
    "[Google Protocol Buffer](https://developers.google.com/protocol-buffers/) format, which\n",
    "is a kind of lightweight XML defintion for serializing structured data in plain text files.  The LeNet network\n",
    "Cafee model is in the data/caffe_models/lenet_model subdirectory.  You can visualize the network using the \n",
    "draw-net.py tool (copied from the Caffe distribution into our cluster repository subdirectory):\n",
    "\n",
    "![LeNet Model](lenet_model.png)\n",
    "\n",
    "The neural network model architecture is defined in the lenet_train_test.prototxt file.  As sown in the diagram,\n",
    "it consists of 5x5 convolutional layer and 2x2 pool layer (conv1, pool1) producing 20 output/feature \n",
    "detectors, followed by another convolutional/pool layer (conv2, pool2), which is also a 5x5 convolution and\n",
    "2x2 pool outputing 50 features from this second convolution.  There is then a fully connected layer with 500\n",
    "units in it (ip1 and relu1, so this layer is using a rectified linear output activation function).  Followed\n",
    "by layer ip2 with 10 output units, representing the final classification, where the ip2 layer uses the Softmax\n",
    "output activation function.\n",
    "\n",
    "The cell below runs the draw-net.py tool to generate the image visualization of the network.  The following\n",
    "cell is running the script from the linux/unix command line.  You may need to modify this cell to run it\n",
    "on a windows machine (e.g. try simply calling python draw-net.py ...):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!./draw-net.py data/caffe_models/lenet_model/lenet_train_test.prototxt lenet_model.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model Training\n",
    "\n",
    "The Caffe solver file for this tutorial is defined to perform 10,000 training iterations on the network.  The next\n",
    "line runs the trainer to create the network model and train it on our mnist lmbd database data.\n",
    "Again this next line is running a command line tool on a linux/unix system, it may not run on your personal\n",
    "laptop unless you have successfully installed caffe, and even then you may need to find the path to the caffe\n",
    "binary or set your system path to include this binary.\n",
    "The statistics from the training on the model and data are saved in the lenet_train.log file.  We can parse\n",
    "this file in the next cell to visualize the performance of the network training.\n",
    "\n",
    "The solver file contains the metaparameters used for the neural network training session.  In this\n",
    "training session we start out with a base learning rate of 0.001 (different from the tutorial), as 0.01\n",
    "was learning a bit too fast on our system and was unstable at times.  We train for a total of 10000 iterations.\n",
    "On our cluster system, this will usually take about 30 minutes to train for 10 iterations, at which point we\n",
    "usually achieve an accuracy of almost 0.99 (99%) on the test data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!caffe train --solver=data/caffe_models/lenet_model/lenet_solver.prototxt 2>&1 | tee data/caffe_models/lenet_model/lenet_train.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize Training Results\n",
    "\n",
    "![Learning Results](lenet_learningcurves.png)\n",
    "\n",
    "The following cell will parse the training performance log produced by caffe and display the training and test\n",
    "loss (e.g. the cost or objective function), as well as the training accuracy (as a ratio between 0.0 and 1.0).\n",
    "Again this cell has commands/scripts meant to be run from the command line.  The first two commands parse the\n",
    "log file for training and testing performance data respectively.  The last command creates the image file\n",
    "which is a standard plot of loss/accuracy over the training iterations of this training run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./parse-caffe-log.py --train data/caffe_models/lenet_model/lenet_train.log > lenet_train.log.train\n",
    "!./parse-caffe-log.py --test data/caffe_models/lenet_model/lenet_train.log > lenet_train.log.test\n",
    "!./plot-learning-curves.py --train lenet_train.log.train --test lenet_train.log.test --outfile lenet_learningcurves.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Prediction on New Data\n",
    "\n",
    "This is not in the original tutorial, but lets try out the trained model on some new data we will create.\n",
    "\n",
    "First of all, in the data/new-mnist-images subdirectory I've added a couple of png images of my own handwriting.\n",
    "Feel free to take some images of your own written digits and also try.  In the next cell we load a single\n",
    "image, then we scale it to be 28x28 size and convert it into the unsigned 8 bit integer single channel values\n",
    "we need for input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<type 'numpy.ndarray'>\n",
      "(900, 872)\n",
      "uint8\n",
      "[[0 0 0 ..., 0 0 0]\n",
      " [0 0 0 ..., 0 0 0]\n",
      " [0 0 0 ..., 0 0 0]\n",
      " ..., \n",
      " [0 0 0 ..., 0 0 0]\n",
      " [0 0 0 ..., 0 0 0]\n",
      " [0 0 0 ..., 0 0 0]]\n"
     ]
    }
   ],
   "source": [
    "# need opencv2 to read in image\n",
    "# by default, reading in as IMREAD_GRAYSCALE will read as unsigned 8 bit values\n",
    "import cv2\n",
    "\n",
    "img_path = 'data/new-mnist-images/derek-image-3.png'\n",
    "#img_path = 'data/new-mnist-images/derek-image-6.png' # some other images to try\n",
    "#img_path = 'data/new-mnist-images/derek-image-8.png' # some other images to try\n",
    "\n",
    "img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)\n",
    "img = (255 - img) # invert black/white, because reversed for some reason in the color png from what we want\n",
    "\n",
    "print type(img)\n",
    "print img.shape\n",
    "print img.dtype\n",
    "print img"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# resize image to desired shape\n",
    "IMAGE_WIDTH=28\n",
    "IMAGE_HEIGHT=28\n",
    "\n",
    "img = cv2.resize(img, (IMAGE_WIDTH, IMAGE_HEIGHT), interpolation=cv2.INTER_CUBIC)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.image.AxesImage at 0x7f5605c96f50>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAP8AAAD8CAYAAAC4nHJkAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAC2RJREFUeJzt3U+oHfUZxvHnaWI36iJpTkOIsVchFILQCIdQUIolUaIU\nohsxC0lBel2kouCiYhd1GUpVuqhCrMG0WKWgYhahJQlCEIp4lTR/TNtYuWLCNfeELIwrm/h2cUe5\nxnv+5Jz5c07e7wcOZ+Y3c++8GfLcmTO/mfNzRAhAPt9pugAAzSD8QFKEH0iK8ANJEX4gKcIPJEX4\ngaQIP5AU4QeSWl7nxlatWhVTU1N1bhJIZXZ2VufOnfMg644UfttbJf1e0jJJf4yIXb3Wn5qa0szM\nzCibBNBDu90eeN2hT/ttL5P0B0l3S9ogabvtDcP+PgD1GuUz/yZJH0bERxHxhaRXJW0rpywAVRsl\n/GslfbJo/nTR9g22p23P2J7pdDojbA5AmSq/2h8RuyOiHRHtVqtV9eYADGiU8J+RtG7R/A1FG4AJ\nMEr435W03vZNtr8r6QFJ+8opC0DVhu7qi4iLtn8p6e9a6OrbExEnSqsMpbAH6vIdS3zLVLVG6ueP\niP2S9pdUC4AacXsvkBThB5Ii/EBShB9IivADSRF+IKlan+fHcCa5r34Uo/67uU+gN478QFKEH0iK\n8ANJEX4gKcIPJEX4gaTo6sNVq1dXId2AHPmBtAg/kBThB5Ii/EBShB9IivADSRF+ICn6+dHTqP3h\n4/o4cr+6MtwHwJEfSIrwA0kRfiApwg8kRfiBpAg/kBThB5IaqZ/f9qykC5IuSboYEe0yisI3TXKf\nc5W1j+s9BJOijJt8fhoR50r4PQBqxGk/kNSo4Q9JB22/Z3u6jIIA1GPU0/7bI+KM7e9LOmD7XxFx\nePEKxR+FaUm68cYbR9wcgLKMdOSPiDPF+7ykNyRtWmKd3RHRjoh2q9UaZXMASjR0+G1fa/v6r6Yl\n3SXpeFmFAajWKKf9qyW9UXS3LJf0l4j4WylVAajc0OGPiI8k/ajEWoAr0u8eAu4D6I2uPiApwg8k\nRfiBpAg/kBThB5Ii/EBShB9IivADSRF+ICnCDyRF+IGkCD+QFOEHkiL8QFIM0Y2JxSO7o+HIDyRF\n+IGkCD+QFOEHkiL8QFKEH0iK8ANJ0c+PsVVlP/4kD3teFo78QFKEH0iK8ANJEX4gKcIPJEX4gaQI\nP5BU3/Db3mN73vbxRW0rbR+wfap4X1FtmZhUtod+oVqDHPlfkrT1srYnJB2KiPWSDhXzACZI3/BH\nxGFJ5y9r3iZpbzG9V9K9JdcFoGLDfuZfHRFzxfSnklaXVA+Amox8wS8WbpLueqO07WnbM7ZnOp3O\nqJsDUJJhw3/W9hpJKt7nu60YEbsjoh0R7VarNeTmAJRt2PDvk7SjmN4h6c1yygFQl0G6+l6R9A9J\nP7R92vZDknZJutP2KUlbinkAE6Tv8/wRsb3Los0l14IGXM396Tyz3xt3+AFJEX4gKcIPJEX4gaQI\nP5AU4QeS4qu7J8DV3B2H5nDkB5Ii/EBShB9IivADSRF+ICnCDyRF+IGk6OefAP0eTb1a7wPgkdxq\nceQHkiL8QFKEH0iK8ANJEX4gKcIPJEX4gaQIP5AU4QeSIvxAUoQfSIrwA0kRfiApwg8kRfiBpPo+\nz297j6SfSZqPiFuKtqck/UJSp1jtyYjYX1WR6O3gwYNdl23ZsqXGSjBJBjnyvyRp6xLtz0bExuJF\n8IEJ0zf8EXFY0vkaagFQo1E+8z9i+6jtPbZXlFYRgFoMG/7nJd0saaOkOUlPd1vR9rTtGdsznU6n\n22oAajZU+CPibERciogvJb0gaVOPdXdHRDsi2q1Wa9g6AZRsqPDbXrNo9j5Jx8spB0BdBunqe0XS\nHZJW2T4t6TeS7rC9UVJImpX0cIU1AqhA3/BHxPYlml+soBYMafPmzV2XNf3d91frmAJXA+7wA5Ii\n/EBShB9IivADSRF+ICnCDyRF+IGkCD+QFOEHkiL8QFKEH0iK8ANJEX4gKcIPJEX4gaQIP5AU4QeS\nIvxAUoQfSIrwA0kRfiApwg8k1feru4Fe+GruycWRH0iK8ANJEX4gKcIPJEX4gaQIP5AU4QeS6ht+\n2+tsv2X7A9snbD9atK+0fcD2qeJ9RfXlom62e74wuQY58l+U9HhEbJD0Y0k7bW+Q9ISkQxGxXtKh\nYh7AhOgb/oiYi4j3i+kLkk5KWitpm6S9xWp7Jd1bVZEAyndFn/ltT0m6VdI7klZHxFyx6FNJq0ut\nDEClBg6/7eskvSbpsYj4bPGyiAhJ0eXnpm3P2J7pdDojFQugPAOF3/Y1Wgj+yxHxetF81vaaYvka\nSfNL/WxE7I6IdkS0W61WGTUDKMEgV/st6UVJJyPimUWL9knaUUzvkPRm+eUBqMogj/TeJulBScds\nHynanpS0S9JfbT8k6WNJ91dTYj0uXbrUc/myZctqqqRedNfl1Tf8EfG2pG7/QzaXWw6AunCHH5AU\n4QeSIvxAUoQfSIrwA0kRfiApvrq7sHx5dbti4e7n6jz33HNdl+3cubPSbY+i6v2C3jjyA0kRfiAp\nwg8kRfiBpAg/kBThB5Ii/EBS9PPXIPMz8/Tljy+O/EBShB9IivADSRF+ICnCDyRF+IGkCD+QFP38\nhX790Zn76nuhH39yceQHkiL8QFKEH0iK8ANJEX4gKcIPJEX4gaT6ht/2Ottv2f7A9gnbjxbtT9k+\nY/tI8bqn+nJRt4jo+cLkGuQmn4uSHo+I921fL+k92weKZc9GxO+qKw9AVfqGPyLmJM0V0xdsn5S0\nturCAFTrij7z256SdKukd4qmR2wftb3H9oouPzNte8b2TKfTGalYAOUZOPy2r5P0mqTHIuIzSc9L\nulnSRi2cGTy91M9FxO6IaEdEu9VqlVAygDIMFH7b12gh+C9HxOuSFBFnI+JSRHwp6QVJm6orE0DZ\nBrnab0kvSjoZEc8sal+zaLX7JB0vvzwAVRnkav9tkh6UdMz2kaLtSUnbbW+UFJJmJT1cSYVjgm4t\nXG0Gudr/tqSlHmbfX345AOrCHX5AUoQfSIrwA0kRfiApwg8kRfiBpAg/kBThB5Ii/EBShB9IivAD\nSRF+ICnCDyRF+IGkXOdz6rY7kj5e1LRK0rnaCrgy41rbuNYlUduwyqztBxEx0Pfl1Rr+b23cnomI\ndmMF9DCutY1rXRK1Daup2jjtB5Ii/EBSTYd/d8Pb72VcaxvXuiRqG1YjtTX6mR9Ac5o+8gNoSCPh\nt73V9r9tf2j7iSZq6Mb2rO1jxcjDMw3Xssf2vO3ji9pW2j5g+1TxvuQwaQ3VNhYjN/cYWbrRfTdu\nI17Xftpve5mk/0i6U9JpSe9K2h4RH9RaSBe2ZyW1I6LxPmHbP5H0uaQ/RcQtRdtvJZ2PiF3FH84V\nEfGrMantKUmfNz1yczGgzJrFI0tLulfSz9XgvutR1/1qYL81ceTfJOnDiPgoIr6Q9KqkbQ3UMfYi\n4rCk85c1b5O0t5jeq4X/PLXrUttYiIi5iHi/mL4g6auRpRvddz3qakQT4V8r6ZNF86c1XkN+h6SD\ntt+zPd10MUtYXQybLkmfSlrdZDFL6Dtyc50uG1l6bPbdMCNel40Lft92e0RslHS3pJ3F6e1YioXP\nbOPUXTPQyM11WWJk6a81ue+GHfG6bE2E/4ykdYvmbyjaxkJEnCne5yW9ofEbffjsV4OkFu/zDdfz\ntXEauXmpkaU1BvtunEa8biL870pab/sm29+V9ICkfQ3U8S22ry0uxMj2tZLu0viNPrxP0o5ieoek\nNxus5RvGZeTmbiNLq+F9N3YjXkdE7S9J92jhiv9/Jf26iRq61HWzpH8WrxNN1ybpFS2cBv5PC9dG\nHpL0PUmHJJ2SdFDSyjGq7c+Sjkk6qoWgrWmottu1cEp/VNKR4nVP0/uuR12N7Dfu8AOS4oIfkBTh\nB5Ii/EBShB9IivADSRF+ICnCDyRF+IGk/g+DR7cUhcj6eAAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f560a0f2e10>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.imshow(img, cmap=plt.cm.gray_r)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import caffe\n",
    "from caffe.proto import caffe_pb2\n",
    "\n",
    "# Read model architecture and the trained model's weights\n",
    "net = caffe.Net('data/caffe_models/lenet_model/lenet_deploy.prototxt',\n",
    "                'data/mnist/lenet_iter_10000.caffemodel',\n",
    "                caffe.TEST)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
