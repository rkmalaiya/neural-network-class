#!/bin/bash
#
# Usage: caffe-dogsvscats-submit.sh []

# -- our name --
#$ -N dharter-caffe-dogsvscats-training
#$ -S /bin/bash

# Make sure that the .e and .o file arrive in the working directory
#$ -cwd

# Merge the standard out and standard error to one file
#$ -j y
WORK_DIR=$USER/repos/neural-network-class/cluster
CAFFE=/home/$USER/anaconda2/bin/caffe

cd $WORK_DIR
/bin/echo "Starting caffe training now.."
/bin/echo "Running on host: `hostname`"
/bin/echo "In directory: `pwd`"
/bin/echo "Starting on: `date`"

$CAFFE train --solver data/caffe_models/caffe_model_1/solver_1.prototxt 2>&1 | tee data/caffe_models/caffe_model_1/model_1_train.log
