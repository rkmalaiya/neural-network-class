#!/usr/bin/env python
"""
plot-learning-curves.py: plot testing and training performance
statistics learning curves.  This script uses 2 tables
of comma separated values of the test and train performance
respectively.  This script can plot either the train
data alone, the test data alone, or if given both, will
plot both train and test data together on same figure.
"""
import argparse
import pandas as pd
import matplotlib
matplotlib.use('Agg') # use a non-rendering backend, so can save images to files
import matplotlib.pyplot as plt
plt.style.use('ggplot')


def plot_learning_curves(train_file=None, test_file=None, out_file='training-performance.png'):
    """Plot the performance of training/testing data.
    This function will plot either just the training
    loss, or the testing loss & accuracy, or all combined
    if asked for.  This function reads the performance
    data from the indicated file of csv values.
    """
    fig = plt.figure(figsize=(8,5)) # 8x5 inches figure by default
    ax1 = fig.add_subplot(111)
    lns = []

    # plot training performance if asked
    if train_file:
        train_log = pd.read_csv(train_file)
        train_loss = ax1.plot(train_log.iteration, train_log.loss,
                             color='red', alpha=0.5, label='Training Loss')
        lns += train_loss

    # plot testing performance if asked
    if test_file:
        test_log = pd.read_csv(test_file)
        test_loss = ax1.plot(test_log.iteration, test_log.loss,
                             linewidth=2, color='green', label='Test Loss')
        lns += test_loss

        ax2 = ax1.twinx()
        test_accuracy = ax2.plot(test_log.iteration, test_log.accuracy,
                                 linewidth=2, color='blue', label='Test Accuracy')
        lns += test_accuracy
        ax2.set_ylabel('Accuracy', fontsize=15)
        ax2.tick_params(labelsize=15)
        ax2.set_ylim(ymin=0, ymax=1)

    # set labels and parameters for main axis
    ax1.set_xlabel('Iterations', fontsize=15)
    ax1.set_ylabel('Loss', fontsize=15)
    ax1.tick_params(labelsize=15)
    ax1.set_ylim(ymin=0, ymax=1)

    # combine lines into single legend
    lbls = [l.get_label() for l in lns]
    ax1.legend(lns, lbls, loc='upper right')

    plt.title('Training/Testing Performance Results')

    # save to a file
    plt.savefig(out_file)


def plot_training_learning_curves():
    """Read in the data of performance statistics on the
    training set data and plot it to a png image file.
    """
    train_file = 'model_1_train.log.train'

    # load data into a pandas data frame
    train_log = pd.read_csv(train_file)

    # plot it to a figure
    fig = plt.figure(figsize=(8, 5)) # 8 x 5 inches figure by default
    ax = fig.add_subplot(111)
    train_loss = ax.plot(train_log.iteration, train_log.loss,
                         color='red', alpha=0.5, label='Training Loss')
    ax.set_xlabel('Iterations', fontsize=15)
    ax.set_ylabel('Loss', fontsize=15)
    ax.tick_params(labelsize=15)
    ax.legend(loc='upper right')
    plt.title('Training Performance', fontsize=18)

    # save to a file
    plt.savefig('training.png')


def plot_testing_learning_curves():
    """Read in the data of performance statistics on the
    testing set data and plot it to a png image file.
    """
    test_file = 'model_1_train.log.test'

    # load data into a pandas data frame
    test_log = pd.read_csv(test_file)

    # plot it to a figure
    fig = plt.figure(figsize=(8, 5)) # 8 x 5 inches figure by default
    ax1 = fig.add_subplot(111)
    test_loss = ax1.plot(test_log.iteration, test_log.loss,
                         linewidth=2, color='green', label='Test Loss')
    ax1.set_xlabel('Iterations', fontsize=15)
    ax1.set_ylabel('Loss', fontsize=15)
    ax1.tick_params(labelsize=15)
    ax1.set_ylim(ymin=0, ymax=1)

    ax2 = ax1.twinx()
    test_accuracy = ax2.plot(test_log.iteration, test_log.accuracy,
                             linewidth=2, color='blue', label='Test Accuracy')
    ax2.set_ylabel('Accuracy', fontsize=15)
    ax2.tick_params(labelsize=15)
    ax2.set_ylim(ymin=0, ymax=1)

    # combine lines from two axis into single legend
    lns = test_loss + test_accuracy
    lbls = [l.get_label() for l in lns]
    ax1.legend(lns, lbls, loc='upper right')

    plt.title('Testing Performance', fontsize=18)

    # save to a file
    plt.savefig('testing.png')


def main():
    description = """Plot training statistics visualization.  This program will take
    in a csv file of loss and/or accuracy information for training and testing
    performance, and plot the results.  This script expects simply the loss per
    iteration for training data, and the loss and accuracy per iteration for test
    data.  It will create a png plot of the asked for data.  It will combine both
    test and training performance in 1 plot if asked to plot both.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-t", "--train", help="Name of CSV file with training data loss information",
                        default=None)
    parser.add_argument("-s", "--test", help="Name of CSV file with testing data loss and accuracy information",
                        default=None)
    parser.add_argument("-o", "--outfile", help="The name of the .png image file the script should create, defaults to learning-performance.png",
                        default="learning-performance.png")
    args = parser.parse_args()

    if not args.train and not args.test:
        parser.print_help()

    plot_learning_curves(train_file=args.train, test_file=args.test, out_file=args.outfile)


if __name__ == '__main__':
    main()
